import os

from django.apps import AppConfig
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from generator.contrib.generate import TextToImageGenerator


class GeneratorConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "generator"
    verbose_name = _("Image generator")

    def ready(self):
        if not os.path.isdir(settings.STATIC_ROOT):
            os.makedirs(settings.STATIC_ROOT)
        if not os.path.isdir(settings.MEDIA_ROOT):
            os.makedirs(settings.MEDIA_ROOT)
        # if self.image_generator is None:
        #     self.image_generator = TextToImageGenerator()

