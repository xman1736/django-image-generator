import re

import os


from django.core.management import BaseCommand
from django.conf import settings

from generator.apps import GeneratorConfig
from generator.contrib.generate import TextToImageGenerator
import time


class Command(BaseCommand):
    help = 'Test'

    def handle(self, *args, **kwargs):
        print(f'Start init model: {time.strftime("%H:%M:%S", time.localtime())}')
        model = GeneratorConfig.image_generator()
        print(f'Model has been initiated: {time.strftime("%H:%M:%S", time.localtime())}')
        img_bytes = model.generate_image('2girl, 1cup')
        print(img_bytes)


