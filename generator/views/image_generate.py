from django.conf import settings
from drf_yasg.utils import swagger_auto_schema

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from generator.rest.permissions import IsValidJWTTokenPermission
from generator.rest.serializers.image import ImageSerializer


class ImageGenerateView(APIView):
    permission_classes = [IsValidJWTTokenPermission]

    @swagger_auto_schema(
        request_body=ImageSerializer,
        responses={
            200: "Successful response - {'user_id': 1, 'image': 'base64_encoded_image_data'}",
            400: "Bad Request - {'success': False, 'fields': {'field_name': ['Error message']}, 'message': 'Failed to "
                 "get data'}"
        },
        operation_description="Generate an image based on provided prompts.",
    )
    def post(self, request, *args, **kwargs):
        serializer = ImageSerializer(data=request.data)
        if serializer.is_valid():
            user_id = serializer.validated_data['user_id']
            positive_prompt = serializer.validated_data['positive_prompt']
            negative_prompt = serializer.validated_data['negative_prompt']

            model = settings.TEXT_TO_IMAGE_MODEL
            img_bytes = model.generate_image(positive_prompt, negative_prompt)
            return Response({'user_id': user_id, 'image': img_bytes}, status=status.HTTP_200_OK)
        
        return Response({'success': False, 'fields': serializer.errors, 'message': 'Failed to get data'},
                        status=status.HTTP_400_BAD_REQUEST)
