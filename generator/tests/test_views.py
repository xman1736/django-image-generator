import requests
import hashlib
import hmac
import json
from urllib.parse import urljoin


from django.conf import settings
from rest_framework.test import APITestCase
from rest_framework import status


class NoDatabaseTestCase(APITestCase):
    databases = {}


class ImageGenerateViewTest(NoDatabaseTestCase):
    def setUp(self):
        self.url = '/api/generate/'

        auth_service_url = urljoin(settings.URL_TO_AUTH_SERVICE, 'api/get-token/')
        data = {
            'user_id': 1
        }
        message = json.dumps(data, separators=(',', ':')).encode('utf-8')
        calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()
        headers = {'Authorization': f'HMAC {calculated_hmac}'}

        response = requests.post(auth_service_url, json=data, headers=headers)
        self.token = response.json()['token']

    def test_image_generation_success(self):
        data = {'user_id': 1, 'positive_prompt': 'Positive', 'negative_prompt': 'Negative'}

        headers = {'Authorization': f'Bearer {self.token}'}
        response = self.client.post(self.url, data, format='json', headers=headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('user_id', response.data)
        self.assertIn('image', response.data)

    def test_image_generation_invalid_data(self):
        data = {'user_id': 1, 'positive_prompt': '', 'negative_prompt': ''}

        headers = {'Authorization': f'Bearer {self.token}'}
        response = self.client.post(self.url, data, format='json', headers=headers)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('fields', response.data)
        self.assertFalse(response.data['success'])
