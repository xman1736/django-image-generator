from django.contrib import admin
from django.urls import path, include
from generator.rest.permissions import IsValidJWTTokenPermission
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Django Image Generator API",
        default_version='v1',
        description="API for generating images",
        contact=openapi.Contact(email="xman1736@gmail.com"),
    ),
    public=True,
    permission_classes=(IsValidJWTTokenPermission,),
)


urlpatterns = [
    path("admin/", admin.site.urls),
    path('api/', include('generator.urls.image_generate')),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
