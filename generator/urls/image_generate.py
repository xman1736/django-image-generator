from django.urls import path
from generator.views import ImageGenerateView


urlpatterns = [
    path('generate/', ImageGenerateView.as_view(), name='generate'),
]
