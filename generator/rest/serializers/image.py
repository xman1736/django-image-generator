from rest_framework import serializers


class ImageSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    positive_prompt = serializers.CharField()
    negative_prompt = serializers.CharField(allow_blank=True)
