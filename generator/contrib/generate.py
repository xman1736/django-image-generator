import base64

from diffusers import StableDiffusionPipeline, DPMSolverMultistepScheduler, AutoencoderKL
import os
from compel import Compel
from PIL import Image
from typing import Optional, Dict
import io
from django.conf import settings
import time


class TextToImageGenerator:
    NUM_INFERENCE_STEPS = 10
    CFG_SCALE = 7.5
    DEFAULT_NEGATIVE_PROMPT = "(worst quality:2), (low quality:2), (normal quality:2), lowres, ((monochrome)), ((grayscale)), bad anatomy, bad hands, mutated, deformed, bad proportions, blurry, distorted, text, signature, logo, username, (mosaic censoring, censored, bar censor), (embedding:badhandv4:1), (embedding:EasyNegative:1), (embedding:EasyNegativeV2:1), signature, artist name, text"

    def __init__(self, model_path=None, vae_path=None):
        path_to_folder = os.path.join(settings.BASE_DIR, 'config', 'image_generator')

        if model_path is None:
            model_path = os.path.join(path_to_folder, 'MHP-v26Fix.safetensors')
        if vae_path is None:
            vae_path = os.path.join(path_to_folder, 'VAE-V3.pt')

        self.vae = AutoencoderKL.from_single_file(vae_path)
        self.pipe = StableDiffusionPipeline.from_single_file(model_path, vae=self.vae)

        self.pipe.scheduler = DPMSolverMultistepScheduler.from_config(self.pipe.scheduler.config)
        # self.pipe.to("cpu")
        self.pipe.safety_checker = None
        self.pipe.requires_safety_checker = False

        self.compel = Compel(tokenizer=self.pipe.tokenizer, text_encoder=self.pipe.text_encoder)

    def __get_prompt(self, positive: str, negative: str) -> Dict:
        if negative == '':
            return {'positive': positive, 'negative': self.DEFAULT_NEGATIVE_PROMPT}
        return {'positive': positive, 'negative': negative}

    @staticmethod
    def __image2bytes(image: Optional[Image.Image]) -> bytes:
        img_byte_arr = io.BytesIO()
        image.save(img_byte_arr, format='PNG')
        return img_byte_arr.getvalue()

    def generate_image(self, positive_prompt: str, negative_prompt='') -> bytes:
        print(f'Generating image: {time.strftime("%H:%M:%S", time.localtime())}')
        prompt = self.__get_prompt(positive_prompt, negative_prompt)
        image = self.pipe(prompt_embeds=self.compel.build_conditioning_tensor(prompt['positive']),
                               negative_prompt_embeds=self.compel.build_conditioning_tensor(prompt['negative']),
                               num_inference_steps=self.NUM_INFERENCE_STEPS,
                               guidance_scale=self.CFG_SCALE).images[0]
        print(f'Image has been generated: {time.strftime("%H:%M:%S", time.localtime())}')
        image_bytes = self.__image2bytes(image)

        return base64.b64encode(image_bytes).decode('utf-8')


